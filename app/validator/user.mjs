import Joi from 'joi';

const options = {
    errors: {
        wrap: {
            label: '',
        },
    },
};

const UserSchema = Joi.object().keys({
    name: Joi.string().min(1).max(30).required(),
    email: Joi.string().min(3).email().optional(),
    mobile: Joi.string().length(10).pattern(/^[0-9]+$/).optional(),
});

async function isValidRequest(data) {
    try {
        const result = await UserSchema.validateAsync(data, options);
        return { error: false, info: '' };
    } catch (err) {
        return { error: true, info: err.details[0]?.message };
    }
}

export { isValidRequest };
