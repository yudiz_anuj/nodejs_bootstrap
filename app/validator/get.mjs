import Joi from 'joi';

const options = {
    errors: {
        wrap: {
            label: '',
        },
    },
};

const UserSchema = Joi.object().keys({
    _id: Joi.string().length(24).required(),
});

async function isValidRequest(data) {
    try {
        const result = await UserSchema.validateAsync(data, options);
        return { error: false, info: '' };
    } catch (err) {
        return { error: true, info: err.details[0]?.message };
    }
}

export { isValidRequest };
