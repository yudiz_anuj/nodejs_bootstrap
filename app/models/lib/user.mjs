import pkg from 'mongoose';

const { Schema, model } = pkg;

const schema = new Schema(
    {
        name: { type: String, min: 1, max: 20, required: true },
        email: { type: String, required: false, default: '' },
        mobile: { type: Number, length: 10, required: false, default: '' },
        qr_id: { type: Schema.Types.ObjectId, ref: 'Qr', required: false, default: null },
    },
    {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    }
);

const User = model('User', schema, 'User');

export { User };
