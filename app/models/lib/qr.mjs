import pkg from 'mongoose';

const { Schema, model } = pkg;

const schema = new Schema(
    {
        qr_string: { type: String, required: false, default: '' },
        video_link: { type: String, required: false, default: '' },
    },
    {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    }
);

const Qr = model('Qr', schema, 'Qr');

export { Qr };
