import { Router } from 'express';
import { userRoute } from './user/index.mjs';

const router = Router();

router.use('/user', userRoute);


export { router };
