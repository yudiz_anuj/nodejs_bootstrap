import mongoose from 'mongoose';
import { Qr, User } from '../../../../models/index.mjs';

async function addQr_txn(qr_data, user_id) {
    const session = await mongoose.startSession();
    let qr, user;
    try {
        await session.withTransaction(
            async () => {
                [qr] = await Qr.create([qr_data], { session });
                if (!qr) throw new Error('qr not created');

                const filter = { _id: user_id };
                const update = { $set: { qr_id: qr._id } };
                const projection = { _id: 0 };
                const options = { new: true, projection };

                user = await User.findOneAndUpdate(filter, update, options).lean();
                if (!user) throw new Error('user not update');
            },
            { writeConcern: { w: 'majority' } }
        );

        return { err: null, data: { qr, user } };
    } catch (error) {
        log.error(`error on transaction_addQr ${error.message}`);
        return { err: error.message, data: {} };
    } finally {
        session.endSession();
    }
}

export { addQr_txn };
