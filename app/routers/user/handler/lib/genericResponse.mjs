async function genericResponse(req, res, next = () => {}) {
    try {
        if (req.data?.err) return res.send(_.genResponse('', req.data.src, req.data.info));
        if (!req.data) return res.send(_.genResponse('', 'INTERNAL_SERVER_ERROR', ''));
        res.send(_.genResponse(req.data._data, 'SUCCESS', ''));
    } catch (err) {
        log.error({ name: err.name, msg: err.message });
        res.send(_.genResponse('', 'INTERNAL_SERVER_ERROR', ''));
    }
}
export { genericResponse };
