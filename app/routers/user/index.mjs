import { Router } from 'express';
import { c } from './controller/index.mjs';
import { h } from './handler/index.mjs';

const router = Router();

router.get('/get', c.get, h.genericResponse);
router.post('/addQr', c.addQr, h.genericResponse);

router.get('/getAll', c.getAll, h.genericResponse);
router.post('/register', c.register, h.genericResponse);

export { router as userRoute };
