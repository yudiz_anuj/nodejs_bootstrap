import { User } from '../../../../models/index.mjs';
import { isValidRequest } from '../../../../validator/user.mjs';

async function register(req, res, next = () => {}) {
    try {
        const body = req.body;
        const { error, info } = await isValidRequest(body);
        if (error) return (req.data = { err: true, info, src: 'JOI_ERROR' });

        const { name, email, mobile } = body;

        const user = await User.create({ name, email, mobile });
        if (!user) return (req.data = { err: true, info: '', src: 'INTERNAL_SERVER_ERROR' });

        req.data = { err: false, _data: user, info: '', src: 'SUCCESS' };
    } catch (error) {
        log.error({ name: error.name, msg: error.message, stack: error.stack });
        req.data = { err: true, info: error.message, src: 'INTERNAL_SERVER_ERROR' };
    } finally {
        return next();
    }
}
export { register };
