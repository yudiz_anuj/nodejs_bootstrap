import { User } from '../../../../models/index.mjs';
import { Mongoose as M } from '../../../../util/index.mjs';
import { addQr_txn } from '../../util/transaction/addQr.mjs';
import { isValidRequest } from '../../../../validator/addQr.mjs';

async function addQr(req, res, next = () => {}) {
    try {
        const body = req.body;
        const { error, info } = await isValidRequest(body);
        if (error) return (req.data = { err: true, info, src: 'JOI_ERROR' });

        const { user_id, qr_string, video_link } = body;

        const user = await User.findOne({ _id: M.mongify(user_id) }).lean();
        if (!user) return (req.data = { err: true, info: '', src: 'ERROR_LOGIN' });

        const txnResult = await addQr_txn({ user_id: M.mongify(user_id), qr_string, video_link }, M.mongify(user_id));
        if (txnResult.err) return (req.data = { err: true, info: '', src: 'TransactionError' });

        req.data = { err: false, _data: txnResult.data, info: '', src: 'SUCCESS' };
    } catch (error) {
        log.error({ name: error.name, msg: error.message, stack: error.stack });
        req.data = { err: true, info: error.message, src: 'INTERNAL_SERVER_ERROR' };
    } finally {
        return next();
    }
}
export { addQr };
