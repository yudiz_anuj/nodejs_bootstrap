import { User } from '../../../../models/index.mjs';

async function getAll(req, res, next = () => {}) {
    try {
        const filter = {};
        const projection = { __v: 0, created_at: 0, updated_at: 0 };

        const userList = await User.find(filter, projection).lean().populate({ path: 'qr_id', select: '_id qr_string video_link' });

        if (!userList) return (req.data = { err: true, info: '', src: 'INTERNAL_SERVER_ERROR' });

        req.data = { err: false, _data: userList, info: '', src: 'SUCCESS' };
    } catch (error) {
        log.error({ name: error.name, msg: error.message, stack: error.stack });
        req.data = { err: true, info: error.message, src: 'INTERNAL_SERVER_ERROR' };
    } finally {
        return next();
    }
}
export { getAll };
