import { User } from '../../../../models/index.mjs';
import { isValidRequest } from '../../../../validator/get.mjs';
import { Mongoose as M } from '../../../../util/index.mjs';

async function get(req, res, next = () => {}) {
    try {
        const body = req.body;
        const { error, info } = await isValidRequest(body);
        if (error) return (req.data = { err: true, info, src: 'JOI_ERROR' });

        const { _id } = body;

        const filter = { _id: M.mongify(_id) };
        const projection = { __v: 0, created_at: 0, updated_at: 0 };

        // method 1
        const user = await User.findOne(filter, projection).lean().populate({ path: 'qr_id', select: '_id qr_string video_link' });

        // method 2
        const [user1] = await User.aggregate([
            { $match: filter },
            {
                $lookup: {
                    from: 'Qr',
                    localField: 'qr_id',
                    foreignField: '_id',
                    as: 'qr_data',
                },
            },
            {
                $project: {
                    _id: 1,
                    'qr_data.created_at': 0,
                    'qr_data.updated_at': 0,
                    'qr_data.__v': 0,
                    created_at: 0,
                    updated_at: 0,
                    __v: 0,
                },
            },
        ]);
        if (!user) return (req.data = { err: true, info: '', src: 'INTERNAL_SERVER_ERROR' });

        req.data = { err: false, _data: user, info: '', src: 'SUCCESS' };
    } catch (error) {
        log.error({ name: error.name, msg: error.message, stack: error.stack });
        req.data = { err: true, info: error.message, src: 'INTERNAL_SERVER_ERROR' };
    } finally {
        return next();
    }
}
export { get };
