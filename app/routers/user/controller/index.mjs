import { get } from './lib/get.mjs';
import { getAll } from './lib/getAll.mjs';
import { addQr } from './lib/addQr.mjs';
import { register } from './lib/register.mjs';

export const c = { register, addQr, get, getAll };
