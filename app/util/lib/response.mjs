export const response = {
    SUCCESS: { code: 1, message: 'Everything worked as expected' }, // - Success
    METHOD_NOT_FOUND: { code: 201, message: 'api_method does not exists' },
    VERSION_NOT_FOUND: { code: 202, message: 'The requested version does not exists' },
    INVALID_REQUEST_METHOD: { code: 203, message: 'The requested request method does not exists' },
    INVALID_AUTH_TOKEN: { code: 204, message: 'The auth token is invalid' },
    RESPONSE_CODE_NOT_FOUND: { code: 205, message: 'The response code does not exists' },
    INVALID_EMAIL: { code: 206, message: 'The email is invalid' },
    //
    PARAMETER_IS_MANDATORY: { code: 207, message: '"paramName" Mandatory Parameter' },
    INVALID_INPUT_EMPTY: { code: 208, message: '"paramName" should not be empty' },
    INVALID_BOOLEAN_INPUT: { code: 209, message: '"paramName" should be a boolean value' },
    PARAMETER_DESCRIPTION_UNDEFINED: { code: 210, message: '"paramName" should have a description' },
    INVALID_INPUT_INTEGER: { code: 210, message: '"paramName" should be a integer' },
    INVALID_INPUT_STRING: { code: 211, message: '"paramName" should be a string' },
    INVALID_STRING_MAX_SIZE: { code: 212, message: '"paramName" length should be a less than size characters' },
    INVALID_STRING_MIN_SIZE: { code: 213, message: '"paramName" length should be a greater than size characters' },
    INVALID_INPUT_INTEGER_MAX: { code: 214, message: '"paramName" should be less than value' },
    INVALID_INPUT_INTEGER_MIN: { code: 215, message: '"paramName" should be greater than value' },
    //
    ERROR_LOGIN: { code: 216, message: 'Invalid Credential, Please try again' },
    CUSTOM_ERROR: { code: 228, message: 'error' },
    //
    USER_NAME_MANDATORY: { code: 229, message: 'User name is mandatory' },
    NAME_ALREADY_TAKEN: { code: 230, message: 'Name is already taken' },
    DEVICE_TOKEN_MANDATORY: { code: 231, message: 'Device token is mandatory' },
    FAILED: { code: 232, message: 'Failed to unlock the card' },
    MAX_CARD_IN_DECK_EXCEEDED: { code: 233, message: 'Already maximum card found in deck' },
    REPLACE_CARD_NOT_FOUND: { code: 234, message: 'Replacing card not found in user deck' },
    CARD_NOT_FOUND_IN_COLLECTION: { code: 235, message: 'Card not found in user collection' },
    GOLD_IS_NOT_ENOUGH: { code: 236, message: 'Gold is not enough to update card level' },
    CARD_IS_NOT_ENOUGH: { code: 237, message: 'Card is not enough to update card level' },
    NO_MORE_CARD_LEVEL_UPGRADE: { code: 238, message: 'Card level is already up-to-date' },
    CRYSTAL_IS_NOT_ENOUGH: { code: 239, message: 'Insufficient crystal in user account' },
    INSUFFICIENT_BALANCE: { code: 239, message: 'Insufficient balance in user account' },
    DAILY_REWARD_CLAIMED: { code: 240, message: 'Daily special offer already claimed' },
    INSUFFICIENT_DECK_COUNT: { code: 241, message: 'Insufficient deck' },
    MAX_DECK_COUNT_REACHED: { code: 241, message: 'Deck count exceeds requirement' },
    INVITE_TOKEN_MANDATORY: { code: 242, message: 'Invite token mandatory' },
    INVALID_INVITE_TOKEN: { code: 243, message: 'Invalid invite token' },
    MAX_INVITE_LIMIT_REACHED: { code: 244, message: 'You have exceeded the maximum invite limit' },
    PLAYER_OFFLINE: { code: 245, message: 'Player is offline' },
    KATHIKA_FAILED: { code: 232, message: 'Failed to unlock the kathika' },
    KATHIKA_ALREADY_FAILED: { code: 232, message: 'Failed to unlock the kathika. Already fetched' },
    KATHIKA_CRYSTAL_FAILED: { code: 232, message: 'Failed to unlock the kathika. Insufficient Crystal' },
    KINGDOM_NOT_CREATED: { code: 232, message: 'Failed to create the kingdom. Incorrect Kingdom Name' },
    KINGDOM_NAME_ALREADY: { code: 232, message: 'Kingdom Name Already Existed, Failed to create the kingdom' },
    KINGDOM_USER_ALREADY: { code: 232, message: 'User Already Existed in Kingdom, Failed to create the kingdom' },
    KINGDOM_USER_ALREADY_EXISTED: { code: 232, message: 'User Already Existed in Kingdom, Failed to join the kingdom' },
    INSUFFICIENT_GOLD: { code: 232, message: 'Insufficient Gold for Create Kingdom, Failed to create the kingdom' },
    INSUFFICIENT_TROPHIES: { code: 232, message: 'Insufficient Trophies/Cups for Create Kingdom, Failed to create the kingdom' },
    REQUEST_LIMIT_EXCEEDED: { code: 232, message: 'ou have exceed the request limit for Kingdom' },
    USER_ALREADY_IN_KINGDOM: { code: 262, message: 'User already in the Kingdom' },
    G_USER_ALREADY_EXISTS: { code: 246, message: 'user already exists' },
    //
    CHARACTER_FAILED: { code: 232, message: 'Failed to unlock the Character' },
    CHARACTER_ALREADY_FAILED: { code: 232, message: 'Failed to unlock the Character. Already fetched' },
    CHARACTER_CRYSTAL_FAILED: { code: 232, message: 'Failed to unlock the Character. Insufficient Crystal/Gems' },
    CHARACTER_GOLD_FAILED: { code: 232, message: 'Failed to unlock the Character. Insufficient Gold' },
    //
    INTERNAL_SERVER_ERROR: { code: 500, message: 'Internal Server Error' },
    JOI_ERROR: { code: 400, message: 'Validation Error' },
    //
    ValidationError: { code: 500, message: 'Internal Server Error' }, // created by mongoose validation
    UserCreationError: { code: 500, message: 'Internal Server Error' }, // created by mongodb
    UserUpdateError: { code: 500, message: 'Internal Server Error' }, // created by mongodb
    DB_FETCH_ERROR: { code: 500, message: 'Internal Server Error' }, // created by mongodb
    DB_UPDATE_ERROR: { code: 500, message: 'Internal Server Error' }, // created by mongodb
    TransactionError: { code: 500, message: 'Internal Server Error' }, // created by mongodb
};
