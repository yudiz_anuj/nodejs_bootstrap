import { logger as log } from './globals/lib/logs.mjs';

process.env.NODE_ENV = 'dev';
process.env.PORT = '3000';
process.env.HOST = 'localhost';

process.env.DB = 'demo';
process.env.DB_USER = '';
process.env.DB_PORT = '27017';
process.env.DB_PASSWORD = '';

process.env.LOG_LEVEL = 'debug';

const oEnv= {
    dev: {
        BASE_URL: `http://${process.env.HOST}:${process.env.PORT}`,
        DB_URL: 'mongodb+srv://evil:1526@cluster.cnn8e.mongodb.net/demo?retryWrites=true&w=majority',
    },
    stag: {
        BASE_URL: `http://${process.env.HOST}:${process.env.PORT}`,
        DB_URL: 'mongodb+srv://evil:1526@cluster.cnn8e.mongodb.net/demo?retryWrites=true&w=majority',
    },
    // prod: {},
};

process.env.BASE_URL = oEnv[process.env.NODE_ENV].BASE_URL;
process.env.DB_URL = oEnv[process.env.NODE_ENV].DB_URL;
process.env.JWT_SECRET = 'jwt-secret';

log.info(`${process.env.NODE_ENV} ${process.env.HOST} configured < / >`);

export {};