import './env.mjs';
import './globals/index.mjs';
import { server } from './server/index.mjs';
import { mongoose } from './app/util/index.mjs';

process.once('uncaughtException', (ex) => {
    log.error(`we have uncaughtException, ${ex.message}, ${ex.stack}`);
    process.exit(1);
});

process.once('unhandledRejection', (ex) => {
    log.error(`we have unhandledRejection, ${ex.message}, ${ex.stack}`);
    process.exit(1);
});

(async () => {
    try {
        await Promise.all([server.initialize(), mongoose.initialize()]);
        log.info(':-)');
    } catch (err) {
        log.info(':-(');
        log.error(`reason: ${err.message}, stack: ${err.stack}`);
    }
})();
