import http from 'http';
import helmet from 'helmet';
import morgan from 'morgan';
import express from 'express';
import compression from 'compression';
import { router } from '../app/routers/index.mjs';


class Server {
    constructor() {}

    async initialize() {
        this.app = express();
        this.httpServer = http.createServer(this.app);
        this.setupMiddleware();
        this.setupServer();
        global.app = this.app;
    }

    setupMiddleware() {
        this.app.use(helmet());
        this.app.use(compression());
        this.app.use(morgan('tiny'));
        this.app.use(express.json({ limit: '10kb' }));
        this.app.use(express.urlencoded({ extended: true }));

        this.app.use((req, res, next) => {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
            res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
            next();
        });

        this.app.use('/', router);
    }

    setupServer() {
        this.httpServer.timeout = 10000;
        this.httpServer.listen(process.env.PORT, () => log.info(`Spinning on ${process.env.PORT} ⚡`));
    }
}

const server = new Server();
export { server };
