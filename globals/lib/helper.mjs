import * as jwt from 'jsonwebtoken';
import { response } from '../../app/util/index.mjs';
const config = {
    JWT_SECRET: process.env.JWT_SECRET,
};

const helper = {
    code: {
        Continue: 100,
        Switching_Protocols: 101,
        Processing: 102,
        Early_Hints: 103,
        OK: 200,
        Created: 201,
        Accepted: 202,
        'Non-Authoritative_Information': 203,
        No_Content: 204,
        Reset_Content: 205,
        Partial_Content: 206,
        'Multi-Status': 207,
        Already_Reported: 208,
        IM_Used: 226,
        Multiple_Choices: 300,
        Moved_Permanently: 301,
        Found: 302,
        See_Other: 303,
        Not_Modified: 304,
        Use_Proxy: 305,
        Temporary_Redirect: 307,
        Permanent_Redirect: 308,
        Bad_Request: 400,
        Unauthorized: 401,
        Payment_Required: 402,
        Forbidden: 403,
        Not_Found: 404,
        Method_Not_Allowed: 405,
        Not_Acceptable: 406,
        Proxy_Authentication_Required: 407,
        Request_Timeout: 408,
        Conflict: 409,
        Gone: 410,
        Length_Required: 411,
        Precondition_Failed: 412,
        Payload_Too_Large: 413,
        URI_Too_Long: 414,
        Unsupported_Media_Type: 415,
        Range_Not_Satisfiable: 416,
        Expectation_Failed: 417,
        "I'm_a_Teapot": 418,
        Misdirected_Request: 421,
        Unprocessable_Entity: 422,
        Locked: 423,
        Failed_Dependency: 424,
        Too_Early: 425,
        Upgrade_Required: 426,
        Precondition_Required: 428,
        Too_Many_Requests: 429,
        Request_Header_Fields_Too_Large: 431,
        Unavailable_For_Legal_Reasons: 451,
        Internal_Server_Error: 500,
        Not_Implemented: 501,
        Bad_Gateway: 502,
        Service_Unavailable: 503,
        Gateway_Timeout: 504,
        HTTP_Version_Not_Supported: 505,
        Variant_Also_Negotiates: 506,
        Insufficient_Storage: 507,
        Loop_Detected: 508,
        Bandwidth_Limit_Exceeded: 509,
        Not_Extended: 510,
        Network_Authentication_Required: 511,
    },
    isObject: (o) => {
        return o instanceof Object && o.constructor === Object;
    },

    isDate: (o) => {
        return o instanceof Object && o.constructor === Date;
    },

    objectWithoutKey: (object = {}, keys = []) => {
        let clone = Object.assign({}, object);
        for (const key of keys) delete clone[key];
        return clone;
    },

    parse: (data) => {
        try {
            return JSON.parse(data);
        } catch (error) {
            return data;
        }
    },

    stringify: (data, offset = 0) => {
        return JSON.stringify(data, null, offset);
    },

    toString: (key) => {
        try {
            return key.toString();
        } catch (error) {
            return '';
        }
    },

    findMinMax: (array) => {
        let nMin = Number.POSITIVE_INFINITY;
        let nMax = Number.NEGATIVE_INFINITY;
        for (const num of array) {
            nMin = nMin > num ? num : nMin;
            nMax = nMax < num ? num : nMax;
        }
        return { nMin, nMax };
    },

    getRandomNumber: (min = 0, max = 100000) => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    clone: (data = {}) => {
        const originalData = data.toObject ? data.toObject() : data; // for mongoose result operations
        const eType = originalData ? originalData.constructor : 'normal';
        if (eType === Object) return { ...originalData };
        if (eType === Array) return [...originalData];
        return data;
    },
    deepClone: (data) => {
        try {
            return helper.parse(helper.stringify(data));
        } catch (error) {
            return data;
        }
    },

    pick: (obj, array ) => {
        const clonedObj = helper.clone(obj);
        return array.reduce((acc, elem) => {
            if (elem in clonedObj) acc[elem] = clonedObj[elem];
            return acc;
        }, {});
    },

    isEmpty: (obj) => {
        if (obj === null || obj === undefined || Array.isArray(obj) || typeof obj !== 'object') {
            return true;
        }
        return Object.getOwnPropertyNames(obj).length === 0;
    },

    randomizeArray: (array) => {
        /* Randomize array in-place using Durstenfeld shuffle algorithm */
        for (let i = array.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            let temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
        return array;
    },

    now: () => {
        const dt = new Date();
        return `[${`${dt}`.split(' ')[4]}:${dt.getMilliseconds()}]`;
    },

    delay: (ttl) => new Promise((resolve) => setTimeout(resolve, ttl)),

    encodeToken: function (payload, options) {
        try {
            return new Promise((res, rej) => {
                if (options) {
                    jwt.sign(payload, config.JWT_SECRET, options, (err, token) => {
                        if (err) return rej(err);
                        res(token);
                    });
                } else {
                    jwt.sign(payload, config.JWT_SECRET, (err, token) => {
                        if (err) return rej(err);
                        res(token);
                    });
                }
            });
        } catch (err) {
            log.error(`Server :> JWT Sign Error , ${err.message}`);
            return Promise.reject(err ? err.message : undefined);
        }
    }, // options: { expiresIn: 30 } // seconds

    decodeToken: function (token, options) {
        try {
            return options ? jwt.decode(token, options) : jwt.decode(token);
        } catch (error) {
            console.log('Server :> JWT decodeToken Error', error.message);
            return error ? error.message : undefined;
        }
    },

    verifyToken: function (token) {
        try {
            return new Promise((res, rej) => {
                jwt.verify(token, config.JWT_SECRET, (err, decoded) => {
                    if (err) {
                        return rej({ status: 'failed', payload: err.message });
                    }
                    res({ status: 'success', payload: decoded });
                });
            });
        } catch (err) {
            return Promise.reject(err ? { status: 'failed', payload: err.message } : { status: 'failed', payload: 'Internal Server Error' });
        }
    }, // jwt expired || invalid signature

    genAckCB: () => {
        return (msg) => {
            // if (debug) log.magenta(`client:-> ${msg}`);
        };
    },

    genResponse: (data, key, msg) => {
        const JOI_ERROR = msg ?? 'Joi Error';
        const responseCode = response[key].code;
        const responseInfo = key === 'JOI_ERROR' ? JOI_ERROR : response[key].message;

        return { responseCode, responseMsg: data ?? {}, responseInfo };
    },
};

export { helper };
