import * as winston from 'winston';
const { combine, timestamp, label, prettyPrint, splat, simple, colorize } = winston.format;

const myCustomLevels = {
    levels: {
        info: 0,
        warn: 1,
        error: 2,
    },
    colors: {
        info: 'green',
        warn: 'yellow',
        error: 'red',
    },
};
const logger = winston.createLogger({
    level: 'info',
    silent: false,
    // format: winston.format.cli(), // winston.format.json()
    // format: combine(label({ label: 'right meow!' }), timestamp(), prettyPrint()),
    // format: combine(splat(), simple()),
    format: combine(winston.format.colorize({ all: true }), winston.format.simple()),
    transports: [new winston.transports.Console()],
});

// logger.error('error');
// logger.warn('warn');
// logger.info('info');
// logger.verbose('verbose');
// logger.debug('debug');
// logger.silly('silly');

export { logger };
