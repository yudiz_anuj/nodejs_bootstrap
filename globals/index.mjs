import { helper } from './lib/helper.mjs';
import { logger } from './lib/logs.mjs';

global._ = helper;
global.log = logger;

export {};
